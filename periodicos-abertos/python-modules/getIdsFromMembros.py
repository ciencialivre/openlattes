"""
    Extrai os ids da página de output do scriptlattes: membros.html
    dependências:
        apt-get install python3-pip
        pip3 install BeautifulSoup4
"""

def getIdsFromMembros(url):
    import requests
    from bs4 import BeautifulSoup
     
    ids=[]
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html,'html5lib')

    tabela_membros = soup.find_all('table')[0]
    for tr in tabela_membros.find_all('tr'):
        tds = tr.find_all('td')
        links = tds[2].find_all('a')
        for link in links:
            if link.has_attr('href'):
                ids.append(link['href'].replace('http://lattes.cnpq.br/',''))
    return(ids)

# Testes: 
#ids=getIdsFromMembros('http://portal.if.usp.br/lattes/ifusp/membros.html')
