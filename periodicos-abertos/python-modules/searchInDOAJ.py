# rodar em python3

def searchInDOAJ(issn,local=False):
    """
    Função que consulta se um dado issn consta no DOAJ - periódicos abertos.
    """
    import csv
    if not local:
        local='/tmp/doaj.csv'
        import requests
        url = 'https://doaj.org/csv'
        response = requests.get(url)
        # Salva em csv. Deve ter solução melhor.
        with open(local,'w') as fout:
            fout.write(response.text) 
  
    # Abrindo csv
    doaj = []
    with open(local,'r') as linhas:
        linhascsv = csv.reader(linhas,delimiter=',')
        for linha in linhascsv:
            doaj.append(linha)

    issn_impressos = [item[3] for item in doaj if item[3]]       
    issn_eletronicos = [item[4] for item in doaj if item[4]]
    issn_doaj = issn_impressos + issn_eletronicos
    issn_doaj = [i.replace('-','') for i in issn_doaj]

    # Limpando o issn do input
    issn = issn.strip()
    issn = issn.replace('-','')

    if issn in issn_doaj:
        return True
    else:
        return False

# Testando:
#print('print sem local periódico aberto: ',searchInDOAJ('18073042'))
#print('print sem local periódico não aberto: ',searchInDOAJ('66661111'))

#print('print com local periódico aberto: ',searchInDOAJ('18073042','/home/thiago/doaj.csv'))
#print('print com local periódico não aberto : ',searchInDOAJ('66661111','/home/thiago/doaj.csv'))
