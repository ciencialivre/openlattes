# python3
import sys
import os
import csv

# Diretório de módulos locais:
sys.path.append(os.path.abspath('../../python-modules'))

# imports
from searchInDOAJ import *

# abre aquivo do doaj
doaj = []
with open('/tmp/doaj.csv') as linhas:
    linhascsv = csv.reader(linhas,delimiter=',')
    for linha in linhascsv:
        doaj.append(linha)

# monta um array com todos issns presentes no doaj
issn_impressos = [item[3] for item in doaj if item[3]]       
issn_eletronicos = [item[4] for item in doaj if item[4]]
issn_doaj = issn_impressos + issn_eletronicos
issn_doaj = [i.replace('-','') for i in issn_doaj]

# produção da FFLCH
fflch_abertos=0

with open('../output/ISSNs_FFLCH.txt') as f:
    fflch = f.readlines()
for line in fflch:
    issn=line.strip().replace('-','')
    if issn in issn_doaj:
        fflch_abertos = fflch_abertos + 1

# produção do IFUSP
ifusp_abertos=0
with open('../output/ISSNs_IFUSP.txt') as f:
    ifusp = f.readlines()
for line in ifusp:
    issn=line.strip().replace('-','')
    if issn in issn_doaj:
        ifusp_abertos = ifusp_abertos + 1

# Escrevendo resultados:
f = open('../output/comparacao-fflch-ifusp.md','w')
f.write("# Comparação de publicação em periódicos abertos entre FFLCH(humanas) e IFUSP(exatas) \n\n")
f.write("Porcentagem de publicações em periódicos aberto no IFUSP entre 2006-2015: \n")
f.write(" - Total de publicações: " + str(len(ifusp)) + "\n")
f.write(" - Total de publicações em periódicos abertos: " + str(ifusp_abertos) + "\n")
f.write(" - Porcentagem de publicações em periódicos abertos: " + str(round((ifusp_abertos/len(ifusp))*100,2)) + " %\n")

f.write("\nPorcentagem de publicações em periódicos aberto na FFLCH entre 2006-2015: \n")
f.write(" - Total de publicações: " + str(len(fflch)) + "\n")
f.write(" - Total de publicações em periódicos abertos: " + str(fflch_abertos) + "\n")
f.write(" - Porcentagem de publicações em periódicos abertos: " + str(round((fflch_abertos/len(fflch))*100,2)) + " %\n")
f.close
