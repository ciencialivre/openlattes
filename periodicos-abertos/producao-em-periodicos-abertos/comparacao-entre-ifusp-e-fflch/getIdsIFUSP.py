# python3
import sys
import os

# Diretório de módulos locais:
sys.path.append(os.path.abspath('../../python-modules'))

# imports
from getIdsFromMembros import *

ids=getIdsFromMembros('http://portal.if.usp.br/lattes/ifusp/membros.html')

f = open('../output/ifusp.list','w')
for id in ids:
    f.write(id+"\n")
f.close
