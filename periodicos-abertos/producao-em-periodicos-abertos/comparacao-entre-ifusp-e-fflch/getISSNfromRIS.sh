#!/bin/bash

# Gera lista de issn do pesquisadores da FFLCH:
cat ../input/ris/USP/FFLCH/FFLCH-publicacoes.ris | grep L3 | cut -c6- | sort > ../output/ISSNs_FFLCH.txt

# Gera lista de issn do pesquisadores do IFUSP:
cat ../input/ris/USP/IFUSP/IFUSP-publicacoes.ris | grep L3 | cut -c6- | sort > ../output/ISSNs_IFUSP.txt
