# Comparação de publicação em periódicos abertos entre FFLCH(humanas) e IFUSP(exatas) 

Porcentagem de publicações em periódicos aberto no IFUSP entre 2006-2015: 
 - Total de publicações: 3987
 - Total de publicações em periódicos abertos: 503
 - Porcentagem de publicações em periódicos abertos: 12.62 %

Porcentagem de publicações em periódicos aberto na FFLCH entre 2006-2015: 
 - Total de publicações: 5154
 - Total de publicações em periódicos abertos: 1533
 - Porcentagem de publicações em periódicos abertos: 29.74 %
