# python3
import csv

# abre aquivo do doaj
doaj = []
with open('/tmp/doaj.csv') as linhas:
    linhascsv = csv.reader(linhas,delimiter=',')
    for linha in linhascsv:
        doaj.append(linha)

# monta um array com todos issns presentes no doaj
issn_impressos = [item[3] for item in doaj if item[3]]       
issn_eletronicos = [item[4] for item in doaj if item[4]]
issn_doaj = issn_impressos + issn_eletronicos
issn_doaj = [i.replace('-','') for i in issn_doaj]

# produção dos CNPq-1A
cnpq1a_abertos=0

with open('../output/ISSNs_cnpq1a.txt') as f:
    cnpq1a = f.readlines()
for line in cnpq1a:
    issn=line.strip().replace('-','')
    if issn in issn_doaj:
        cnpq1a_abertos = cnpq1a_abertos + 1

# Escrevendo resultados:
f = open('../output/cnpq1a.md','w')
f.write("# Publicação em periódicos abertos do pesquisadores CNPq-1A entre 2006-2015 \n\n")
f.write(" - Total de publicações: " + str(len(cnpq1a)) + "\n")
f.write(" - Total de publicações em periódicos abertos: " + str(cnpq1a_abertos) + "\n")
f.write(" - Porcentagem de publicações em periódicos abertos: " + str(round((cnpq1a_abertos/len(cnpq1a))*100,2)) + " %\n")

f.close
