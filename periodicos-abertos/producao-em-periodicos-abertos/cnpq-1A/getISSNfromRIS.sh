#!/bin/bash

# Gera lista de issn do pesquisadores CNPq-1A:
cat ../input/ris/CNPq-1A/cnpq1a-publicacoes.ris | grep L3 | cut -c6- | sort > ../output/ISSNs_cnpq1a.txt
