Criado durante o evento [*Ciência Aberta 2015*](http://www.cienciaaberta.net/encontro2015/) 
no [GHC](http://garoa.net.br/).

Objetivos: 

- Quantificar a porcentagem dos periódicos avaliados no Qualis são de acesso aberto;
- Avaliar a produção de grupos de pesquisadores em periódicos de acesso aberto;
