Quantidade total de periódicos avaliados pela CAPES: 
13805

Quantidade total de periódicos no DOAJ: 
10751

Quantidade total de periódicos impressos avaliados pelo Qualis que são de acesso aberto: 
1965

Quantidade total de periódicos eletrônicos avaliados pelo Qualis que são de acesso aberto 
739

Porcentagem global (periódicos impressos + eletrônicos) de periódicos avaliados pelo Qualis que são de acesso aberto: 
19.59 %

Periódicos avaliados pelo Qualis que são de acesso aberto, 
mas que foram contados duas vezes (impresso e eletrônico), pois o Qualis o faz assim.
415

Porcentagem global (periódicos impressos + eletrônicos) de periódicos avaliados pelo 
Qualis que são de acesso aberto, porém removendo-se os duplicados.
16.58 %

Precisamos rever a questão dos duplicados, pois ...

